﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSystem.Library
{
    public class PlayerFactory
    {
        public IPlayer CreatePlayer(string company)
        {
            switch (company)
            {
                case "SF" : 
                    return new SFPlayer();
                case "Netflix" :
                    return new NetflixPlayer();
                default :
                    return null;
            }
        }
    }
}
