﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSystem.Library
{
    public class MovieFacade
    {
        private IPlayer Player { get; set; }
        public bool HasBeenPlayed;

        public MovieFacade(IPlayer player)
        {
            HasBeenPlayed = false;
            Player = player;
        }

        public void Play(string movieName)
        {
            Player.PlayMovie(movieName);
            HasBeenPlayed = true;
        }
    }
}
